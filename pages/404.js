import React from 'react';
import Link from 'next/link';

import dictionary from '../langs/es/404.json';

const Custom404 = () => {
    return (
        <div className="error-area">
            <div className="d-table">
                <div className="d-table-cell">
                    <div className="container">
                        <div className="error-content">
                            <img src="/img/error.png" alt="image" />
                            <h3>{dictionary.title}</h3>
                            <p>{dictionary.content}</p>

                            <div className="btn-box">
                                <Link href="/">
                                    <a className="default-btn">
                                        <i className="flaticon-history"></i> 
                                        {dictionary.button_back} <span></span>
                                    </a>
                                </Link>
                                <Link href="/">
                                    <a className="default-btn">
                                        <i className="flaticon-earth-day"></i> 
                                        {dictionary.button_home} <span></span>
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Custom404;