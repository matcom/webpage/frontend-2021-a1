import React from 'react';
import Navbar from '../../components/_App/Navbar';
import PageBanner from '../../components/Common/PageBanner';
import ProjectHuechunContent from '../../components/ProjectHuechun/ProjectHuechunContent';
//import RelatedProjects from '../../components/ProjectHuechun/RelatedProjects';
import Footer from '../../components/_App/Footer';

const ProjectHuechun = () => {
    return (
        <React.Fragment>
            <Navbar />
            <PageBanner
                pageTitle="Proyecto Relave" 
                homePageText="Proyectos" 
                homePageUrl="/" 
                activePageText="Relave" 
            />
            <ProjectHuechunContent />
            {/* <RelatedProjects /> */}
            <Footer />
        </React.Fragment>
    );
}

export default ProjectHuechun;