import React from 'react';
import Navbar from '../../components/_App/Navbar';
import PageBanner from '../../components/Common/PageBanner';
import ProjectOftukuContent from '../../components/ProjectOftuku/ProjectOftukuContent';
//import RelatedProjects from '../../components/ProjectOftuku/RelatedProjects';
import Footer from '../../components/_App/Footer';

const ProjectOftuku = () => {
    return (
        <React.Fragment>
            <Navbar />
            <PageBanner
                pageTitle="Proyecto Oftuku" 
                homePageText="Proyectos" 
                homePageUrl="/" 
                activePageText="Oftuku" 
            />
            <ProjectOftukuContent />
            {/* <RelatedProjects /> */}
            <Footer />
        </React.Fragment>
    );
}

export default ProjectOftuku;