import React from 'react';
import Navbar from '../../components/_App/Navbar';
import PageBanner from '../../components/Common/PageBanner';
import ServiceOftukuContent from '../../components/ServiceDetails/ServiceDetailsContent';
import Footer from '../../components/_App/Footer';

const ProjectOftuku = () => {
    return (
        <React.Fragment>
            <Navbar />
            <PageBanner
                pageTitle="Oftuku" 
                homePageText="Proyectos" 
                homePageUrl="/" 
                activePageText="SaaS" 
            />
            <ServiceOftukuContent />
            <Footer />
        </React.Fragment>
    );
}

export default ProjectOftuku;