import React from 'react';
import Navbar from '../components/_App/Navbar';
import MainBanner from '../components/Index/MainBanner2';
//import OurServices from '../components/Index/OurServices';
//import OurProducts from '../components/Index/OurProducts';
import AboutArea from '../components/Index/AboutArea';
import OurMission from '../components/Index/OurMission';
//import Funfacts from '../components/Index/Funfacts';
import HowItWork from '../components/Index/HowItWork';
//import History from '../components/Index/History';
//import RecentProjects from '../components/Index/RecentProjects';
import TeamMembers from '../components/Index/TeamMembers';
//import Testimonials from '../components/Index/Testimonials';
//import StartProject from '../components/Index/StartProject';
//import ContactInfo from '../components/Index/ContactInfo';
import ContactForm from '../components/Index/ContactForm';
import Footer from '../components/_App/Footer';

const Index = () => {
	return (
		<React.Fragment>
			<Navbar whiteStyle={true} />
			<MainBanner />
			<AboutArea />
			{/* <Funfacts /> */}
			{/* <OurServices /> */}
			<OurMission />
			<HowItWork />
			<TeamMembers />
			{/* <StartProject /> */}
			{/* <ContactForm /> */}
			<Footer />
		</React.Fragment>
	);
}

export default Index;