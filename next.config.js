const path = require('path');

module.exports = {
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')],
    },
    webpack: (config, options) => {
        config.module.rules.push({
            test: /\.glsl/,
            use: [
                options.defaultLoaders.babel,
                'shader-loader'
            ],
        });
        return config;
    },
}
