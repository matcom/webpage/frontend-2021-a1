import React from 'react';
//import Link from 'next/link';

const LoadingSpinner = () => {
    return (
        <div className="loading-spinner">
            <div className="ellipsis"></div>
            <div className="ellipsis"></div>
            <div className="ellipsis"></div>
            <div className="ellipsis"></div>
        </div>
    )
}

export default LoadingSpinner;