import React from 'react';
//import Link from 'next/link';
import Sidebar from './ProjectSidebar';
import VtkViewer from '../VtkViewer/VtkViewer';

const ProjectContent = () => {
	return (
		<section className="case-studies-details-area ptb-100">
			<div className="container">
				<div className="row">
					<div className="col-lg-8 col-md-12">
						<div className="case-studies-details-image">
							<VtkViewer zoom={1.75} />
							{/* <div className="video">
								<iframe src="https://www.youtube.com/embed/ewUeulnkU9o" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
							</div>
							<img src="/img/projects/projects-details1.jpg" alt="image" /> */}
						</div>
						<div className="case-studies-details-desc">
							<span className="sub-title">Pronóstico &amp; Diagnóstico</span>
							<h3>Material Particulado dos Fuentes</h3>
							<p>Nuestro Modelo de Dispersión de Contaminantes (Modelo), desarrollado íntegramente 
								por MatCom en Chile, cuenta con nuevas herramientas matemáticas que la distingue 
								de soluciones existentes y, junto con la arquitectura en la nube de nuestro desarrollo 
								(Cloud Computing), permitirá una innovación digital fácil de acoplar y listas para el uso.
							</p>
							<br />
							<p> 
								Su complejidad viene dada por la concatenación de un sistema de información 
								geográfico, un modelo meteorológico regional(b), un modelo de viento micro-escala 
								local 3D (c) y un modelo de dispersión de partículas(d); permitiendo que los 
								datos generados/simulados tengan una escala espacial desde metros y una escala 
								temporal adaptable de minutos a horas, agregando valor en operación, sustentabilidad y RSE. 
								Con las consideraciones de escenarios de topografía compleja en Chile, 
								la meteorológica que considera el modelo de viento y la validación de los 
								primeros resultados de las simulaciones, el Modelo vincula los 4 (a,b,c,d) 
								componentes en un prototipo funcional de mínima escala que puede ser adaptado 
								a las necesidades específicas del cliente. 
							</p>

                            <div className="row align-items-center">
								<div className="col-lg-6 col-md-6">
									<div className="icon-box">
										<i className="flaticon-analytics"></i>
									</div>
									<span>Desafío</span>
									<p>Comportamiento <br />
										local del polvo <br />
										en terreno complejo</p>
								</div>
								<div className="col-lg-6 col-md-6">
									<p>Debido a la complejidad de la orografía de la zona, los modelos de tipo puff
										no representan correctamente el comportamiento del PM.
									</p>
								</div>
                            </div>
                            <div className="row align-items-center">
								<div className="col-lg-6 col-md-6">
									<div className="icon-box">
										<i className="flaticon-analysis"></i>
									</div>
									<span>Solución</span>
									<p>ARD-3D</p>
								</div>
								<div className="col-lg-6 col-md-6">
									<p>Hemos desarrollado un nuevo modelo CFD para lograr solucionar el difícil comportamiento del polvo
										en terrenos complejos, ARD-3D. Junto con nuestro viento local, resulta ser el mejor aliado para
										representar el PM 10.
									</p>
								</div>
                            </div>

							<h3>Proceso</h3>
							<p>Nuestra metodología considera 4 pasos, te los presentamos a continuación</p>
							<div className="tabs-content">
								<div className="tab active-tab" id="tab-1">
								<div className="content-box">
									<figure className="image-box">
										<img src="" alt="" />
									</figure>
									<div className="text">
										<h4>Paso 1: Faena y Fuentes</h4>
										<p>Se determina la zona de interés, geo-localizan las fuentes y las tasas de emision de cada una</p>
									</div>
								</div>
								</div>
								<div className="tab" id="tab-2">
								<div className="content-box">
									<figure className="image-box">
										<img src="" alt="" />
									</figure>
									<div className="text">
									<h4>Paso 2: Modelo Meteorológico WRF</h4>
									<p>Se ejecuta el modelo para un pronóstico de hasta 120 horas o diagnóstico de días, meses u años anteriores
									</p>
									</div>
								</div>
								</div>
								<div className="tab" id="tab-3">
								<div className="content-box">
									<figure className="image-box">
										<img src="" alt="" />
									</figure>
									<div className="text">
									<h4>Paso 3: Viento local</h4>
									<p>Simulamos el comportamiento en 3D para el viento ajustado a la orografia de la zona de interés, 
										para ello utilizamos el modelo HDWind® de nuestros Partners SINUMCC con los datos obtenidos del paso 2.
									</p>
									</div>
								</div>
								</div>
								<div className="tab" id="tab-4">
								<div className="content-box">
									<figure className="image-box">
										<img src="" alt="" />
									</figure>
									<div className="text">
									<h4>Paso 4: ARD-3D </h4>
									<p>Dispersión Material Particulado. Con los datos obtenidos del paso 3, ejecutamos nuestro innovador modelo tipo CFD, para
										simular el comportamiento del material particulado en la zona de interés.</p>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>

					<div className="col-lg-4 col-md-12">
						<Sidebar />
					</div>
				</div>
			</div>
		</section>
	)
}

export default ProjectContent;