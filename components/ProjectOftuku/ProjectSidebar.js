import React from 'react';
//import Link from 'next/link';

const CaseStudiesSidebar = () => {
    return (
        <div className="case-studies-sidebar-sticky">
            <div className="case-studies-details-info">
                <ul>
                    <li>
                        <div className="icon">
                            <i className='bx bx-user-pin'></i>
                        </div>
                        <span>Cliente:</span>
                        <a href="https://www.cmp.cl/" target="_blank">Compa&ntilde;&iacute;a Minera del Pac&iacute;fico</a>
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-map'></i>
                        </div>
                        <span>Ubicación:</span>
                        Planta Pellets de Huasco
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-purchase-tag'></i>
                        </div>
                        <span>Tecnologías:</span>
                        Python, Data Science, VTK
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-check'></i>
                        </div>
                        <span>Publicación:</span>
                        28 Junio 2020
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-globe'></i>
                        </div>
                        <span>Sitio Web:</span>
                        <a href="https://www.cmp.cl/" target="_blank">cmp.cl</a>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default CaseStudiesSidebar;