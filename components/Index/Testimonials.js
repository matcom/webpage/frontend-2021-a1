import React from 'react';
import Link from 'next/link';
import dynamic from 'next/dynamic';
const OwlCarousel = dynamic(import('react-owl-carousel3'));

import dictionary from '../../langs/es/index.json';

const options = {
    loop: true,
    nav: true,
    dots: false,
    autoplayHoverPause: true,
    autoplay: true,
    margin: 30,
    navText: [
        "<i class='flaticon-left-1'></i>",
        "<i class='flaticon-right-1'></i>"
    ],
    responsive: {
        0: {
            items: 1,
        },
        768: {
            items: 2,
        },
        992: {
            items: 2,
        }
    }
};

const Testimonials = () => {
    const [display, setDisplay] = React.useState(false);

    React.useEffect(() => {
        setDisplay(true);
    }, [])

    return (
        <section className="testimonials-area bg-f1f8fb">
            <div className="container">
                <div className="section-title">
                    <span className="sub-title">
                        <img src="/img/star-icon.png" alt="image" /> 
                        {dictionary.testimonials_label}
                    </span>
                    <h2>{dictionary.testimonials_title}</h2>
                    <p>{dictionary.testimonials_content}</p>
                </div>

                {display && (
                    <OwlCarousel 
                        className="testimonials-slides owl-carousel owl-theme"
                        {...options}
                    > 
                        {dictionary.testimonials_comments.map((comment, index) => (
                            <div key={index} className="single-testimonials-item">
                                <p>{comment.commentary}</p>

                                <div className="client-info">
                                    <div className="d-flex justify-content-center align-items-center">
                                        <img src={comment.image} alt="image" />
                                        <div className="title">
                                            <h3>{comment.name}</h3>
                                            <span>{comment.title}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </OwlCarousel>
                )}

                <div className="testimonials-view-btn text-center">
                    <Link href="/testimonials">
                        <a className="default-btn">
                            <i className="flaticon-view"></i> 
                             {dictionary.testimonials_button} <span></span>
                        </a>
                    </Link>
                </div>
            </div>

            <div className="shape-img1">
                <img src="/img/shape/shape1.svg" alt="image" />
            </div>

            <div className="divider white-color"></div>
        </section>
    )
}

export default Testimonials;