import React from 'react';
//import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const ContactInfo = () => {
    return (
        <div className="contact-info-area pt-100 pb-70">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6">
                        <div className="contact-info-box">
                            <div className="back-icon">
                                <i className='bx bx-map'></i>
                            </div>
                            <div className="icon">
                                <i className='bx bx-map'></i>
                            </div>
                            <h3>{dictionary.contact_info_address.title}</h3>
                            <p className="selectable">{dictionary.contact_info_address.content}</p>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="contact-info-box">
                            <div className="back-icon">
                                <i className='bx bx-phone-call'></i>
                            </div>
                            <div className="icon">
                                <i className='bx bx-phone-call'></i>
                            </div>
                            <h3>{dictionary.contact_info_contact.title}</h3>
                            {dictionary.contact_info_contact.content.map((item, index) => (
                                <p key={index}>{item[0]}: <a href={item[1]}>{item[2]}</a></p>
                            ))}
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6 offset-lg-0 offset-md-3">
                        <div className="contact-info-box">
                            <div className="back-icon">
                                <i className='bx bx-time-five'></i>
                            </div>
                            <div className="icon">
                                <i className='bx bx-time-five'></i>
                            </div>
                            <h3>{dictionary.contact_info_hours.title}</h3>
                            {dictionary.contact_info_hours.content.map((item, index) => (
                                <p key={index}>{item}</p>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContactInfo;