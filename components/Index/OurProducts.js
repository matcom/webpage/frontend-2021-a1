import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const OurServices = () => {
    return (
        <section className="services-area ptb-100 bg-e3fbff">
            <div className="container">
                <div className="section-title">
                    <span className="sub-title">
                        <img src="/img/star-icon.png" alt="image" draggable="false" /> 
                        {dictionary.our_products_label}
                    </span>
                    <h2>{dictionary.our_products_title}</h2>
                    {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>*/}
                </div>

                <div className="row">
                    {dictionary.our_products_services.map((service, index) => (
                        <div key={index} className="col-lg-4 col-md-6 col-sm-6">
                            <div className="single-services-item">
                                <div className="icon">
                                    <img src={service.image} alt="image" draggable="false" />
                                </div>
                                <h3>
                                    <Link href="/service-details-two">
                                        <a>{service.title}</a>
                                    </Link>
                                </h3>
                                <p>{service.content}</p>
                                    
                                <Link href="/service-details-two">
                                    <a className="default-btn">
                                        <i className="flaticon-right"></i> 
                                        {dictionary.our_products_button} <span></span>
                                    </a>
                                </Link>

                                <div className="shape1">
                                    <img src="/img/services/service-shape1.png" alt="image" draggable="false" />
                                </div>
                                <div className="shape2">
                                    <img src="/img/services/service-shape2.png" alt="image" draggable="false" />
                                </div>
                            </div>
                        </div>
                    ))}

                    <div className="col-lg-12 col-md-12">
                        <div className="load-more-btn text-center">
                            <Link href="/services-five">
                                <a className="default-btn">
                                    <i className="flaticon-refresh"></i> 
                                    {dictionary.our_products_button2} <span></span>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default OurServices;