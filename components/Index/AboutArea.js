import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const AboutArea = () => {
    return (
        <section className="about-area pb-100" id="about-us">
            <div className="container-fluid">
                <div className="row align-items-center">
                    <div className="col-lg-6 col-md-12">
                        <div className="about-img">
                            <img src="/img/banner-two-main-img.png" alt="image" />
                            <div className="shape">
                                <img src="/img/about/about-shape1.png" alt="image" draggable="false" />
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-12">
                        <div className="about-content">
                            <div className="content">
                                <span className="sub-title">
                                    <img src="/img/star-icon.png" alt="image" draggable="false" /> 
                                    {dictionary.about_us_label}
                                </span>
                                <h2>{dictionary.about_us_title}</h2>
                                <p>{dictionary.about_us_content}</p>

                                <ul className="about-list">
                                    {dictionary.about_us_tools.map((tool, index) => (
                                        <li key={index}>
                                            <i className="flaticon-tick"></i>
                                            {tool}
                                            <img src="/img/our-mission/our-mission-shape2.png" alt="image" draggable="false" />
                                        </li>
                                    ))}
                                </ul>
                                {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>*/}
                                
                                {/* <Link href="/about-us-two">
                                    <a className="default-btn">
                                        <i className="flaticon-right"></i> 
                                        {dictionary.about_us_button}
                                        <span></span>
                                    </a>
                                </Link> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="circle-shape1">
                <img src="/img/shape/circle-shape1.png" alt="image" draggable="false" />
            </div>
        </section>
    )
}

export default AboutArea;