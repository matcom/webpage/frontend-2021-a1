import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const RecentProjects = () => {
    return (
        <section className="projects-area bg-color pt-100 pb-70">
            <div className="container">
                <div className="section-title">
                    <span className="sub-title">
                        <img src="/img/star-icon.png" alt="image" /> {dictionary.recent_projects_label}
                    </span>
                    <h2>{dictionary.recent_projects_title}</h2>
                    <p>{dictionary.recent_projects_content}</p>
                </div>

                <div className="row">
                    {dictionary.recent_projects_items.map((project, index) => (
                        <div className="col-lg-4 col-md-6" key={index}>
                            <div className="single-projects-box">
                                <div className="image">
                                    <img src={project.image} alt="image" />

                                    <Link href="/case-studies-details">
                                        <a className="link-btn"><i className='bx bx-plus'></i></a>
                                    </Link>
                                </div>

                                <div className="content">
                                    <h3>
                                        <Link href="/case-studies-details">
                                            <a>{project.title}</a>
                                        </Link>
                                    </h3>
                                    <span>{project.category}</span>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}

export default RecentProjects;