import React from 'react';

import dictionary from '../../langs/es/index.json';

const Funfacts = () => {
    return (
        <div className="funfacts-area pb-70">
            <div className="container">
                <div className="row">
                    {dictionary.funfacts.map((fact, index) => (
                        <div key={index} className="col-lg-3 col-sm-6 col-6 col-md-3">
                            <div className="single-funfacts-box">
                                <div className="icon">
                                    <img src={fact.image} alt="image" />
                                </div>
                                <h3>{fact.title}</h3>
                                <p>{fact.content}</p>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Funfacts;