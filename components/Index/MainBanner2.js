import React from 'react';
import ReactWOW from 'react-wow';
//import ModalVideo from 'react-modal-video';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const MainBanner = () => {
    const [isOpen, setIsOpen] = React.useState(true);
    const openModal = () => {
        setIsOpen(!isOpen);
    }

    return (
        <React.Fragment>
            {/* If you want to change the video need to update videoID
            <ModalVideo 
                channel='youtube' 
                isOpen={!isOpen} 
                videoId='bk7McNUjWgw' 
                onClose={() => setIsOpen(!isOpen)} 
            /> */}

            <div className="main-banner2">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-5 col-md-12">
                            <div className="main-banner-content">
                                <ReactWOW delay='.1s' animation='fadeInLeft'>
                                    <h1>{dictionary.banner_title}</h1>
                                </ReactWOW>
                                
                                <ReactWOW delay='.1s' animation='fadeInLeft'>
                                    <p>{dictionary.banner_content}</p>
                                </ReactWOW>

                                <ReactWOW delay='.1s' animation='fadeInRight'>
                                    <div className="btn-box">
                                        <Link href="/#about-us">
                                            <a className="default-btn">
                                                <i className="flaticon-structure"></i> 
                                                {dictionary.banner_button1} <span></span>
                                            </a>
                                        </Link>

                                        {/* <Link href="#play-video">
                                            <a
                                                onClick={e => {e.preventDefault(); openModal()}}
                                                className="video-btn popup-youtube"
                                            > 
                                                <i className="flaticon-google-play"></i> {dictionary.banner_button2}
                                            </a>
                                        </Link> */}
                                    </div>
                                </ReactWOW>
                            </div>
                        </div>

                        {/* <div className="col-lg-7 col-md-12">
                            <div className="main-banner-animation-image">
                                <img src="/img/banner1.svg" alt="image" />
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default MainBanner;