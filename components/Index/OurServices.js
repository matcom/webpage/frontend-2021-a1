import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const OurServices = () => {
    return (
        <section className="services-area ptb-100">
            <div className="container">
                <div className="section-title">
                    <h2>{dictionary.services_title}</h2>
                    <p>{dictionary.services_content}</p>
                </div>

                <div className="row">
                    {dictionary.services_items.map((service, index) => (
                        <div key={index} className="col-lg-4 col-md-6 col-sm-12">
                            <div className="single-services-box-item">
                                <div className="icon">
                                    <i className={service.icon}></i>
                                </div>
                                <h3>
                                    <Link href="/service-details">
                                        <a>{service.title}</a>
                                    </Link>
                                </h3>
                                <p>{service.content}</p>
                                <Link href="/service-details">
                                    <a className="learn-more-btn">
                                        <i className="flaticon-right"></i> 
                                        Learn More
                                    </a>
                                </Link>
                                <div className="shape">
                                    <img src="/img/services/service-shape4.png" alt="image" draggable="false" />
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}

export default OurServices;