import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const TeamMembers = () => {
    return (
        <section className="scientist-area bg-color pt-100 pb-70">
            <div className="container">
                <div className="section-title">
                    <span className="sub-title">
                        <img src="/img/star-icon.png" alt="image" /> 
                        {dictionary.team_label}
                    </span>
                    <h2>{dictionary.team_title}</h2>
                    <p>{dictionary.team_content}</p>
                </div>

                <div className="row">
                    {dictionary.team_members.map((member, index) => (
                        <div key={index} className="col-lg-3 col-md-6">
                            <div className="single-scientist-item-box">
                                <div className="image-container">
                                    <div className="image">
                                        <img src={member.photo} alt="image" />

                                        {/*(!!member.socials)&&(
                                            <ul className="social">
                                                {member.socials.map((social, index) => (
                                                    <li key={index}>
                                                        <Link href={social.url}>
                                                            <a className="d-block" target="_blank">
                                                                <i className={'bx bxl-' + social.type}></i>
                                                            </a>
                                                        </Link>
                                                    </li>
                                                ))}
                                            </ul>
                                                )*/}
                                    </div>
                                </div>
                                <div className="content">
                                    <h3>{member.name}</h3>
                                    <span>{member.role}</span>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}

export default TeamMembers;