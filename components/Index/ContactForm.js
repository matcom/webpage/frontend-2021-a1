import React from 'react';

import dictionary from '../../langs/es/index.json';

const ContactForm = () => {
	const [data, setData] = React.useState({
		name: '',
		email: '',
		phone_number: '',
		message: '',
	});

	const handleSubmit = (event) => {
		event.preventDefault();

		fetch('/api/send', {
			method: 'POST',
			body: JSON.stringify(data),
			headers: { 'Content-type': 'application/json; charset=UTF-8' }
		}).then(response => {
			if (response.ok) return response.json();
			return Promise.reject(response);
		}).then(data => {
			if(data.result == 'ok'){
				alert(dictionary.contact_feedback_ok);
				setData({
					name: '',
					email: '',
					phone_number: '',
					message: '',
				});
			}else{
				alert(dictionary.contact_feedback_error);
			}
		}).catch(() => {
			alert(dictionary.contact_feedback_error);
			console.warn('Something went wrong.');
		});
	};

	const handleInput = (event) => {
		let elem = event.target;
		if(elem.name in data){
			setData({
				...data,
				[elem.name]: elem.value,
			});
		}
	};

	return (
		<section className="contact-area pb-100" id="contact">
			<div className="container">
				<div className="section-title">
					<span className="sub-title">
						<img src="/img/star-icon.png" alt="image" /> 
						{dictionary.contact_form_label}
					</span>
					<h2>{dictionary.contact_form_title}</h2>
					<p>{dictionary.contact_form_content}</p>
				</div>

				<div className="row">
					<div className="col-lg-6 col-md-12">
						<div className="contact-image">
							<img src="/img/contact.png" alt="image" />
						</div>
					</div>

					<div className="col-lg-6 col-md-12">
						<div className="contact-form">
							<form id="contactForm" onSubmit={handleSubmit}>
								<div className="row">
									<div className="col-lg-12 col-md-6">
										<div className="form-group">
											<input type="text" name="name" onChange={handleInput} value={data.name} className="form-control" id="name" required placeholder={dictionary.contact_form_field_name} />
										</div>
									</div>

									<div className="col-lg-12 col-md-6">
										<div className="form-group">
											<input type="email" name="email" onChange={handleInput} value={data.email} className="form-control" id="email" required placeholder={dictionary.contact_form_field_email}  />
										</div>
									</div>

									<div className="col-lg-12 col-md-12">
										<div className="form-group">
											<input type="text" name="phone_number" onChange={handleInput} value={data.phone_number} className="form-control" id="phone_number" placeholder={dictionary.contact_form_field_phone}  />
										</div>
									</div>

									<div className="col-lg-12 col-md-12">
										<div className="form-group">
											<textarea name="message" id="message" onChange={handleInput} value={data.message} className="form-control" cols="30" rows="6" required placeholder={dictionary.contact_form_field_message} ></textarea>
										</div>
									</div>

									<div className="col-lg-12 col-md-12">
										<button type="submit" className="default-btn">
											<i className="flaticon-tick"></i> 
											Send Message <span></span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}

export default ContactForm;