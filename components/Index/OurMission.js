import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const OurMission = () => {
    return (
        <div className="our-mission-area ptb-100">
            <div className="container-fluid">
                <div className="row align-items-center">
                    <div className="col-lg-6 col-md-12">
                        <div className="our-mission-content">
                            <div className="content">
                                <h2>{dictionary.our_mission_title}</h2>
                                <p>{dictionary.our_mission_content}</p>
                                <ul className="features-list">
                                    {dictionary.our_mission_items.map((item, index) => (
                                        <li key={index}><i className="flaticon-tick"></i> {item}</li>
                                    ))}
                                </ul>
                                {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>*/}

                                {/*<Link href="/membership-levels">
                                    <a className="default-btn">
                                        <i className="flaticon-right"></i> 
                                        Apply Now <span></span>
                                    </a>
                                </Link>*/}
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-12">
                        <div className="our-mission-image">
                            <img src="/img/our-mission/mision.png" alt="image" />
                            <div className="shape">
                                <img src="/img/our-mission/our-mission-shape1.png" alt="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default OurMission;