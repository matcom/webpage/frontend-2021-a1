import React from 'react';
import Link from 'next/link';

import dictionary from '../../langs/es/index.json';

const StartProject = () => {
    return (
        <div className="project-start-area ptb-100">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6 col-md-12">
                        <div className="project-start-image">
                            <img src="/img/project-start1.png" alt="image" />
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-12">
                        <div className="project-start-content">
                            <h2>{dictionary.start_project_title}</h2>
                            <p>{dictionary.start_project_content}</p>
                            <Link href="/contact">
                                <a className="default-btn">
                                    <i className="flaticon-web"></i>
                                    {dictionary.start_project_button} <span></span>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>

            <div className="vector-shape3">
                <img src="/img/shape/vector-shape3.png" alt="image" />
            </div>
        </div>
    )
}

export default StartProject;