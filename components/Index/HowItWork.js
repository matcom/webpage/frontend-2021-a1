import React from 'react';

import dictionary from '../../langs/es/index.json';

const HowItWork = () => {
    return (
        <section className="how-its-work-area ptb-100">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6 col-md-12">
                        <div className="how-its-work-image">
                            <img src="/img/how-its-work.png" alt="image" />
                        </div>
                    </div>

                    <div className="col-lg-6 col-md-12">
                        <div className="how-its-work-content">
                            <span className="sub-title">
                                <img src="/img/star-icon.png" alt="image" /> 
                                {dictionary.how_it_work_label}
                            </span>
                            <h2>{dictionary.how_it_work_title}</h2>
                            {/*<p>{dictionary.how_it_work_content}</p>*/}

                            <div className="inner-box">
                                {dictionary.how_it_work_steps.map((step, index) => (
                                    <div key={index} className="single-item">
                                        <div className="count-box">{index + 1}</div>
                                        <h3>{step.title}</h3>
                                        <p>{step.content}</p>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HowItWork;