import React from 'react';

import dictionary from '../../langs/es/index.json';

const OurHistory = () => {
    return (
        <section className="history-area ptb-100 bg-fafafb">
            <div className="container">
                <div className="section-title">
                    <span className="sub-title">
                        <img src="/img/star-icon.png" alt="image" /> 
                        {dictionary.history_label}
                    </span>
                    <h2>{dictionary.history_title}</h2>
                </div>

                <ol className="timeline history-timeline">
                    {dictionary.history_events.map((event, index) => (
                        <li key={index} className="timeline-block">
                            <div className="timeline-date">
                                <span>{event.year}</span>
                                {event.date}
                            </div>

                            <div className="timeline-icon">
                                <span className="dot-badge"></span>
                            </div>

                            <div className="timeline-content">
                                <div className="row align-items-center">
                                    <div className="col-lg-7 col-md-12">
                                        <div className="content">
                                            <h3>{event.title}</h3>
                                            <p>{event.content}</p>
                                        </div>
                                    </div>

                                    <div className="col-lg-5 col-md-12">
                                        <div className="image">
                                            <img src={event.image} alt="image" draggable="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    ))}
                </ol>
            </div>
        </section>
    )
}

export default OurHistory;