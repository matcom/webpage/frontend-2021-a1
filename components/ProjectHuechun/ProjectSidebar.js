import React from 'react';
//import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CaseStudiesSidebar = () => {
    return (
        <div className="case-studies-sidebar-sticky">
            <div className="case-studies-details-info">
                <ul>
                    <li>
                        <div className="icon">
                            <FontAwesomeIcon icon="project-diagram" />
                        </div>
                        <span>Aplicaci&oacute;n:</span>
                        Dep&oacute;sito de Relaves
                    </li>
                    <li>
                        <div className="icon">
                            <FontAwesomeIcon icon="map-marker-alt" />
                        </div>
                        <span>Ubicaci&oacute;n:</span>
                        Tranque ovejer&iacute;a Huech&uacute;n
                    </li>
                    <li>
                        <div className="icon">
                            <FontAwesomeIcon icon="tags" />
                        </div>
                        <span>Tecnologías:</span>
                        Python, Data Science, VTK
                    </li>
                    <li>
                        <div className="icon">
                            <FontAwesomeIcon icon="calendar-check" />
                        </div>
                        <span>Fecha Diagn&oacute;stico:</span>
                        2014
                    </li>
                </ul>
                {/* <ul>
                    <li>
                        <div className="icon">
                            <i className='bx bx-user-pin'></i>
                        </div>
                        <span>Cliente:</span>
                        <a href="https://www.example.cl/" target="_blank">Example</a>
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-map'></i>
                        </div>
                        <span>Ubicación:</span>
                        Calle N° 675,<br/>Cuidad
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-purchase-tag'></i>
                        </div>
                        <span>Tecnologías:</span>
                        Python, Data Science, VTK
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-check'></i>
                        </div>
                        <span>Publicación:</span>
                        01 Mes 2020
                    </li>
                    <li>
                        <div className="icon">
                            <i className='bx bx-globe'></i>
                        </div>
                        <span>Sitio Web:</span>
                        <a href="https://www.example.cl/" target="_blank">Example.cl</a>
                    </li>
                </ul> */}
            </div>
        </div>
    )
}

export default CaseStudiesSidebar;