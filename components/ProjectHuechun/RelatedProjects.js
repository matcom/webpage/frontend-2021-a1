import React from 'react';
import Link from 'next/link';

const RelatedProjects = () => {
    return (
        <section className="services-area pt-100 pb-70 bg-f1f8fb">
            <div className="container">
                <div className="section-title">
                    <h2>More Services You Might Like</h2>
                </div>

                <div className="row">
                    <div className="col-lg-4 col-md-6">
                        <div className="single-projects-box">
                            <div className="image">
                                <img src="/img/projects/project1.jpg" alt="image" />

                                <Link href="/case-studies-details">
                                    <a className="link-btn"><i className='bx bx-plus'></i></a>
                                </Link>
                            </div>

                            <div className="content">
                                <h3>
                                    <Link href="/case-studies-details">
                                        <a>Movie Recommendation</a>
                                    </Link>
                                </h3>
                                <span>System Project</span>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="single-projects-box">
                            <div className="image">
                                <img src="/img/projects/project2.jpg" alt="image" />

                                <Link href="/case-studies-details">
                                    <a className="link-btn"><i className='bx bx-plus'></i></a>
                                </Link>
                            </div>

                            <div className="content">
                                <h3>
                                    <Link href="/case-studies-details">
                                        <a>Customer Segmentation</a>
                                    </Link>
                                </h3>
                                <span>Machine Learning</span>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-4 col-md-6">
                        <div className="single-projects-box">
                            <div className="image">
                                <img src="/img/projects/project3.jpg" alt="image" />

                                <Link href="/case-studies-details">
                                    <a className="link-btn"><i className='bx bx-plus'></i></a>
                                </Link>
                            </div>

                            <div className="content">
                                <h3>
                                    <Link href="/case-studies-details">
                                        <a>Data Analysis</a>
                                    </Link>
                                </h3>
                                <span>Web Project</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default RelatedProjects;