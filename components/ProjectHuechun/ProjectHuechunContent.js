import React from 'react';
//import Link from 'next/link';
import Sidebar from './ProjectSidebar';
import VtkViewerSingle from '../VtkViewer/VtkViewerSingle';

const ProjectContent = () => {
	return (
		<section className="case-studies-details-area ptb-100">
			<div className="container">
				<div className="row">
					<div className="col-lg-8 col-md-12">
						<div className="case-studies-details-image">
							<VtkViewerSingle file="/vtk/huechun" zoom={1.1} />
						</div>
						<div className="case-studies-details-desc">
							<span className="sub-title">Identificación de zonas de riesgo</span>
							<h3>Levantamiento de polvo en tranque de relaves</h3>
							<p>
								Estudios diagnósticos de zonas con mayor riesgo de levantamiento de polvo en un
								depósito de relaves mineros.
							</p>
							<p> 
								Este tipo de estudios puede ser usado para optimizar la operación del tranque de
								relaves, tomando decisiones basadas en datos para dar un mejor uso a los recursos.
							</p>
							<p>
								En la imagen se simula las zonas de riesgo del Tranque Ovejería ubicado en Huechún,
								haciendo uso de datos públicos.
							</p>
						</div>
					</div>

					<div className="col-lg-4 col-md-12">
						<Sidebar />
					</div>
				</div>
			</div>
		</section>
	)
}

export default ProjectContent;