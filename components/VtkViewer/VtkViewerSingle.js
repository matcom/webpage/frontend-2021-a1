import React from 'react';
//import Link from 'next/link';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner.js';
//import { importScene } from './utils.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const leftButton = 1;
const middleButton = 2;
const rightButton = 3;

async function customInteractor(interactorStyle){
    interactorStyle.removeAllMouseManipulators();
    
    const Rotate = (await import('vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRotateManipulator')).default;
    const Pan = (await import('vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballPanManipulator')).default;
    const ZoomToMouse = (await import('vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomToMouseManipulator')).default;

	// LeftButton behaviour
	const leftManipulator = Rotate.newInstance();
	leftManipulator.setButton(leftButton);
	interactorStyle.addMouseManipulator(leftManipulator);

	// RightButton behaviour
	const rightManipulator = Pan.newInstance();
	// const oldOnMouseMove = rightManipulator.onMouseMove;
	// rightManipulator.onMouseMove = (interactor, renderer, position) => {
	// 	interactorStyle.setCenterOfRotation([7150, 4600, 281.19866943359375]);
	// 	oldOnMouseMove(interactor, renderer, position);
	// };
	//console.log(rightManipulator);
	rightManipulator.setButton(rightButton);
	interactorStyle.addMouseManipulator(rightManipulator);

	// MiddleButton/Scroll behaviour
	const middleManipulator = ZoomToMouse.newInstance();
	middleManipulator.setButton(middleButton);
	middleManipulator.setScrollEnabled(true);
	interactorStyle.addMouseManipulator(middleManipulator);

    // Always add gesture
    const vtkGestureCameraManipulator = (await import('vtk.js/Sources/Interaction/Manipulators/GestureCameraManipulator')).default;
	interactorStyle.addGestureManipulator(vtkGestureCameraManipulator.newInstance());
}

async function initViewer(container){
    const viewer = {};
    const vtkGenericRenderWindow = (await import('vtk.js/Sources/Rendering/Misc/GenericRenderWindow')).default;
    const vtkInteractorStyleManipulator = (await import('vtk.js/Sources/Interaction/Style/InteractorStyleManipulator')).default;

    // Objects
	const genericRenderer = vtkGenericRenderWindow.newInstance();
	genericRenderer.setContainer(container);
	genericRenderer.setBackground([0.1, 0.1, 0.1]);
	genericRenderer.resize();

	const interactorStyle = vtkInteractorStyleManipulator.newInstance();
	interactorStyle.setCenterOfRotation([7150, 4600, 281.19866943359375]);
	genericRenderer.getInteractor().setInteractorStyle(interactorStyle);
	await customInteractor(interactorStyle);

	viewer.renderer = genericRenderer.getRenderer();
	viewer.renderWindow = genericRenderer.getRenderWindow();
	viewer.genericRenderer = genericRenderer;
	viewer.camera = viewer.renderer.getActiveCamera();
	viewer.camera.setFocalPoint(7540, 2817, 52);

	// Some config
	viewer.renderer.setLightFollowCamera(false);
	viewer.renderer.setUseShadows(false);
	viewer.renderer.removeAllLights();
	
	// Actors Storage
	viewer.actors = [];

	return viewer;
};

const importScene = async (viewerObject, url) => {
	const vtkHttpSceneLoader = (await import('vtk.js/Sources/IO/Core/HttpSceneLoader')).default;
	return new Promise((resolve) => {
		const sceneImporter = vtkHttpSceneLoader.newInstance({ renderer: viewerObject.renderer });
		sceneImporter.setUrl(url);
		sceneImporter.onReady(() => {
			let scenes = sceneImporter.getScene();
			return resolve({
				//scenes: sceneImporter.getScene(),
				actors: scenes.map(scene => scene.actor),
			});
		});
	});
};

const fixLights = (viewerObject) => {
	let lights = viewerObject.renderer.getLights();
	for(let light of lights){
		light.setLightTypeToSceneLight();
		light.setDirectionAngle(0, 15);
	}
};

const parseActors = async (actors) => {
	let index = 0;
	// for(let actor of actors){
	// 	let mapper = actor.getMapper();
	// 	let lookupTable = mapper.getLookupTable();
	// 	console.log(lookupTable);
	// 	lookupTable.setSaturationRange([1, 1]);

	// 	if(index == 0){
	// 		// White
	// 		lookupTable.setHueRange([1, 1]);
	// 	}
	// 	else if(index == 1){
	// 		lookupTable.setHueRange([0.10, 0.10]);
	// 	}
	// 	else if(index == 2){
	// 		lookupTable.setHueRange([0.20, 0.20]);
	// 	}
	// 	else if(index == 3){
	// 		// Black
	// 		lookupTable.setHueRange([0.30, 0.30]);
	// 	}
	// 	else if(index == 4){
	// 		// Black
	// 		lookupTable.setHueRange([0.40, 0.40]);
	// 	}
	// 	else if(index == 5){
	// 		// Black
	// 		lookupTable.setHueRange([0.50, 0.50]);
	// 	}
	// 	else if(index == 6){
	// 		// Black
	// 		lookupTable.setHueRange([0.60, 0.60]);
	// 	}
	// 	else if(index == 7){
	// 		// Black
	// 		lookupTable.setHueRange([0.70, 0.70]);
	// 	}
	// 	else if(index == 8){
	// 		// Black
	// 		lookupTable.setHueRange([0.80, 0.80]);
	// 	}
	// 	else if(index == 9){
	// 		// Black
	// 		lookupTable.setHueRange([0.90, 0.90]);
	// 	}
	// 	else{
	// 		// Black
	// 		lookupTable.setValueRange([0, 0]);
	// 	}

	// 	// Increase Index
	// 	++index;
	// }
};

const VtkViewerSingle = ({ file, zoom = 1 }) => {
    const viewerElement = React.createRef();
    const [isLoading, setLoading] = React.useState(true);
	const [viewer, setViewer] = React.useState(null);

	const removeActors = (viewer) => {
		let actors = viewer.actors.splice(0, viewer.actors.length);
		for(let actor of actors) viewer.renderer.removeActor(actor);
	};

	const resetCamera = async () => {
		if(!viewer) return;

		removeActors(viewer);

		let result = await importScene(viewer, file);
		parseActors(result.actors);

		viewer.renderer.setBackground([0.1333, 0.0863, 0.2196]);
		viewer.renderer.resetCamera();
		viewer.camera.zoom(zoom);
		viewer.camera.elevation(-15);

		viewer.renderWindow.render();
		fixLights(viewer);
		viewer.renderWindow.render();
	};

    React.useEffect(() => {
		setLoading(true);

		(async function(){
			if(!viewer){
				let viewer = await initViewer(viewerElement.current);
				setViewer(viewer);

				let result = await importScene(viewer, file);
				parseActors(result.actors);

				viewer.renderer.setBackground([0.1333, 0.0863, 0.2196]);
				viewer.renderer.resetCamera();
				viewer.camera.zoom(zoom);
				viewer.camera.elevation(-15);

				viewer.renderWindow.render();
				fixLights(viewer);
				viewer.renderWindow.render();
			}
			else{
				removeActors(viewer);

				let result = await importScene(viewer, file);
				parseActors(result.actors);

				viewer.renderer.resetCamera();
				viewer.renderer.setBackground([0.1333, 0.0863, 0.2196]);
				viewer.camera.zoom(zoom);
				viewer.camera.elevation(-15);
				viewer.renderWindow.render();
			}

			setLoading(false);
		})().catch(console.error);
    }, [file]);

    return (
        <div className="vtk-viewer">
            <div className="vtk-viewer-content">
                {(isLoading)&&(
                    <div className="loading">
                        <LoadingSpinner />
                        <span className="loading-label">Cargando Modelo...</span>
                    </div>
                )}
                <div className="vtk-viewer-frame" ref={viewerElement}></div>
            </div>
			<div className="actions">
				<button onClick={resetCamera}>
					<FontAwesomeIcon icon="expand" />
				</button>
			</div>
        </div>
    )
}

export default VtkViewerSingle;