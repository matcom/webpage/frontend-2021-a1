import vtkHttpSceneLoader from 'vtk.js/Sources/IO/Core/HttpSceneLoader';
import vtkGenericRenderWindow from 'vtk.js/Sources/Rendering/Misc/GenericRenderWindow';
import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
//import vtkHttpDataSetReader from 'vtk.js/Sources/IO/Core/HttpDataSetReader';
//import vtkPixelSpaceCallbackMapper from 'vtk.js/Sources/Rendering/Core/PixelSpaceCallbackMapper';
import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
//import vtkTexture from 'vtk.js/Sources/Rendering/Core/Texture';
import vtkLabelWidget from 'vtk.js/Sources/Interaction/Widgets/LabelWidget';
//import vtkConeSource from 'vtk.js/Sources/Filters/Sources/ConeSource';
import vtkCylinderSource from 'vtk.js/Sources/Filters/Sources/CylinderSource';
//import vtkSphereMapper from 'vtk.js/Sources/Rendering/Core/SphereMapper';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';

import vtkMouseCameraTrackballMultiRotateManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballMultiRotateManipulator';
import vtkMouseCameraTrackballPanManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballPanManipulator';
import vtkMouseCameraTrackballRollManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRollManipulator';
import vtkMouseCameraTrackballRotateManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRotateManipulator';
import vtkMouseCameraTrackballZoomManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomManipulator';
import vtkMouseCameraTrackballZoomToMouseManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomToMouseManipulator';
import vtkGestureCameraManipulator from 'vtk.js/Sources/Interaction/Manipulators/GestureCameraManipulator';

//import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';

const leftButton = 1;
const middleButton = 2;
const rightButton = 3;

const manipulatorFactory = {
	None: null,
	Pan: vtkMouseCameraTrackballPanManipulator,
	Zoom: vtkMouseCameraTrackballZoomManipulator,
	Roll: vtkMouseCameraTrackballRollManipulator,
	Rotate: vtkMouseCameraTrackballRotateManipulator,
	MultiRotate: vtkMouseCameraTrackballMultiRotateManipulator,
	ZoomToMouse: vtkMouseCameraTrackballZoomToMouseManipulator,
};

export function customInteractor(interactorStyle){
	interactorStyle.removeAllMouseManipulators();

	// LeftButton behaviour
	const leftManipulator = manipulatorFactory.Rotate.newInstance();
	leftManipulator.setButton(leftButton);
	interactorStyle.addMouseManipulator(leftManipulator);

	// RightButton behaviour
	const rightManipulator = manipulatorFactory.Pan.newInstance();
	// const oldOnMouseMove = rightManipulator.onMouseMove;
	// rightManipulator.onMouseMove = (interactor, renderer, position) => {
	// 	interactorStyle.setCenterOfRotation([7150, 4600, 281.19866943359375]);
	// 	oldOnMouseMove(interactor, renderer, position);
	// };
	//console.log(rightManipulator);
	rightManipulator.setButton(rightButton);
	interactorStyle.addMouseManipulator(rightManipulator);

	// MiddleButton/Scroll behaviour
	const middleManipulator = manipulatorFactory.ZoomToMouse.newInstance();
	middleManipulator.setButton(middleButton);
	middleManipulator.setScrollEnabled(true);
	interactorStyle.addMouseManipulator(middleManipulator);

	// Always add gesture
	interactorStyle.addGestureManipulator(
		vtkGestureCameraManipulator.newInstance()
	);
}

export function initViewer(container){
	const viewer = {};

	// Objects
	const genericRenderer = vtkGenericRenderWindow.newInstance();
	genericRenderer.setContainer(container);
	genericRenderer.setBackground([0.1, 0.1, 0.1]);
	genericRenderer.resize();

	const interactorStyle = vtkInteractorStyleManipulator.newInstance();
	interactorStyle.setCenterOfRotation([7150, 4600, 281.19866943359375]);
	genericRenderer.getInteractor().setInteractorStyle(interactorStyle);
	customInteractor(interactorStyle);

	viewer.renderer = genericRenderer.getRenderer();
	viewer.renderWindow = genericRenderer.getRenderWindow();
	viewer.genericRenderer = genericRenderer;
	viewer.camera = viewer.renderer.getActiveCamera();
	viewer.camera.setFocalPoint(7540, 2817, 52);

	// Some config
	viewer.renderer.setLightFollowCamera(false);
	viewer.renderer.removeAllLights();

	return viewer;
};

const receptors = {
	huasco_ii: {
		coord: [9197.23, 4419.21, 47.155],
		label: 'Huasco II',
		color: [0, 1, 0],
	},
	skate_park: {
		coord: [11386.4, 4573.2, 47.3632],
		label: 'Skate Park',
		color: [0, 1, 0],
	},
	poblacion_huasco: {
		coord: [9844.67, 4415.26, 61.8301],
		label: 'Población Huasco 2',
		color: [0, 1, 0],
	},
	escuela_jose_miguel_carrera: {
		coord: [10333.3, 4166.99, 70.7492],
		label: 'Escuela José Miguel Carrera',
		color: [0, 1, 0],
	},
	hospital: {
		coord: [10495.5, 4741.86, 45.6958],
		label: 'Hospital',
		color: [0, 1, 0],
	},
	municipalidad: {
		coord: [10047.1, 4702.02, 40.1882],
		label: 'Municipalidad',
		color: [0, 1, 0],
	},
	escuela_english_college: {
		coord: [9439.51, 4829.86, 48.733],
		label: 'Escuela English College',
		color: [0, 1, 0],
	},
};

export async function asignReceptorColors(receptorColors){
	for(let receptor of receptorColors){
		if(receptor.name in receptors){
			if(receptor.color == 'green'){
				receptors[receptor.name].color = [0, 1, 0];
			}
			else if(receptor.color == 'yellow'){
				receptors[receptor.name].color = [1, 1, 0];
			}
			else if(receptor.color == 'red'){
				receptors[receptor.name].color = [1, 0, 0];
			}
		}
	}
};

export async function importScene(viewerObject, url){
	return new Promise((resolve, reject) => {
		const sceneImporter = vtkHttpSceneLoader.newInstance({ renderer: viewerObject.renderer });
		sceneImporter.setUrl(url);
		sceneImporter.onReady(() => {
			let scenes = sceneImporter.getScene();
			return resolve({
				//scenes: sceneImporter.getScene(),
				actors: scenes.map(scene => scene.actor),
			});
		});
	});
};

export async function createReceptors(viewerObject){
	return new Promise((resolve, reject) => {
		const actors = [];
		for(let receptor_name in receptors){
			// Create Cylinder
			const cylinderSource = vtkCylinderSource.newInstance({
				direction: [0, 0, 1],
				center: receptors[receptor_name].coord,
				resolution: 15,
				height: 20,
				radius: 50,
			});
			const actor = vtkActor.newInstance();
			const mapper = vtkMapper.newInstance();

			actor.setMapper(mapper);
			actor.getProperty().setRepresentation(2);
			actor.getProperty().setColor(...receptors[receptor_name].color);
			actors.push(actor);

			mapper.setInputConnection(cylinderSource.getOutputPort());

			viewerObject.renderer.addActor(actor);
		}

		viewerObject.renderWindow.render();

		return resolve({ actors });
	});
};

export async function createReceptorLabels(viewerObject){
	return new Promise((resolve, reject) => {
		const labels = [];
		for(let receptor_name in receptors){
			// Create label
			const widget = vtkLabelWidget.newInstance();
			widget.setInteractor(viewerObject.renderWindow.getInteractor());
			widget.setEnabled(1);
			widget.getWidgetRep().setLabelText(receptors[receptor_name].label);
			widget.getWidgetRep().setWorldPosition(receptors[receptor_name].coord);
			labels.push(widget);
		}

		viewerObject.renderWindow.render();

		return resolve({ labels });
	});
};