import React from 'react';
//import Link from 'next/link';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner.js';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { importScene } from './utils.js';

const TOTAL_SCENES = 12;

const generateOptions = () => {
	const options = [];
	for(let value = 0; value < TOTAL_SCENES; ++value){
		options.push(
			<option key={value} value={value}>Tiempo #{value + 1}</option>
		);
	};
	return options;
};

const leftButton = 1;
const middleButton = 2;
const rightButton = 3;

async function customInteractor(interactorStyle){
    interactorStyle.removeAllMouseManipulators();
    
    const Rotate = (await import('vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRotateManipulator')).default;
    const Pan = (await import('vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballPanManipulator')).default;
    const ZoomToMouse = (await import('vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomToMouseManipulator')).default;

	// LeftButton behaviour
	const leftManipulator = Rotate.newInstance();
	leftManipulator.setButton(leftButton);
	interactorStyle.addMouseManipulator(leftManipulator);

	// RightButton behaviour
	const rightManipulator = Pan.newInstance();
	// const oldOnMouseMove = rightManipulator.onMouseMove;
	// rightManipulator.onMouseMove = (interactor, renderer, position) => {
	// 	interactorStyle.setCenterOfRotation([7150, 4600, 281.19866943359375]);
	// 	oldOnMouseMove(interactor, renderer, position);
	// };
	//console.log(rightManipulator);
	rightManipulator.setButton(rightButton);
	interactorStyle.addMouseManipulator(rightManipulator);

	// MiddleButton/Scroll behaviour
	const middleManipulator = ZoomToMouse.newInstance();
	middleManipulator.setButton(middleButton);
	middleManipulator.setScrollEnabled(true);
	interactorStyle.addMouseManipulator(middleManipulator);

    // Always add gesture
    const vtkGestureCameraManipulator = (await import('vtk.js/Sources/Interaction/Manipulators/GestureCameraManipulator')).default;
	interactorStyle.addGestureManipulator(vtkGestureCameraManipulator.newInstance());
}

async function initViewer(container){
    const viewer = {};
    const vtkGenericRenderWindow = (await import('vtk.js/Sources/Rendering/Misc/GenericRenderWindow')).default;
    const vtkInteractorStyleManipulator = (await import('vtk.js/Sources/Interaction/Style/InteractorStyleManipulator')).default;

    // Objects
	const genericRenderer = vtkGenericRenderWindow.newInstance();
	genericRenderer.setContainer(container);
	genericRenderer.setBackground([0.1, 0.1, 0.1]);
	genericRenderer.resize();

	const interactorStyle = vtkInteractorStyleManipulator.newInstance();
	interactorStyle.setCenterOfRotation([7150, 4600, 281.19866943359375]);
	genericRenderer.getInteractor().setInteractorStyle(interactorStyle);
	await customInteractor(interactorStyle);

	viewer.renderer = genericRenderer.getRenderer();
	viewer.renderWindow = genericRenderer.getRenderWindow();
	viewer.genericRenderer = genericRenderer;
	viewer.camera = viewer.renderer.getActiveCamera();
	viewer.camera.setFocalPoint(6187.5, 5947.5, 239.35218811035156);

	// Some config
	viewer.renderer.setLightFollowCamera(false);
	viewer.renderer.setUseShadows(false);
	viewer.renderer.removeAllLights();
	
	// Actors Storage
	viewer.actors = [];

	return viewer;
};

const importScene = async (viewerObject, url) => {
	const vtkHttpSceneLoader = (await import('vtk.js/Sources/IO/Core/HttpSceneLoader')).default;
	return new Promise((resolve) => {
		const sceneImporter = vtkHttpSceneLoader.newInstance({ renderer: viewerObject.renderer });
		sceneImporter.setUrl(url);
		sceneImporter.onReady(() => {
			let scenes = sceneImporter.getScene();
			return resolve({
				//scenes: sceneImporter.getScene(),
				actors: scenes.map(scene => scene.actor),
			});
		});
	});
};

const fixLights = (viewerObject) => {
	let lights = viewerObject.renderer.getLights();
	for(let light of lights){
		light.setLightTypeToSceneLight();
		light.setDirectionAngle(0, 15);
	}
};

const importTerrain = async (viewerObject) => {
	let result = await importScene(viewerObject, '/vtk/oftuku/terrain');
	for(let actor of result.actors){
		actor.setScale(1, 1, 3);

		let mapper = actor.getMapper();
		let lookupTable = mapper.getLookupTable();
		lookupTable.setHueRange([0, 0]);
		lookupTable.setSaturationRange([0, 0]);
		lookupTable.setValueRange([1, 1]);
	}
};

const zeropadNumber = (value) => (('000' + value).substr(-3));

const importPlume = async (viewerObject, index) => {
	let location = `/vtk/oftuku/scene_plume_${zeropadNumber(index)}`;
	let result = await importScene(viewerObject, location);
	for(let actor of result.actors){
		viewerObject.actors.push(actor);

		let mapper = actor.getMapper();
		let lookupTable = mapper.getLookupTable();
		lookupTable.setHueRange([0, 0]);
		lookupTable.setSaturationRange([0, 0]);
		lookupTable.setValueRange([1, 1]);
	}
};

const importFloor = async (viewerObject, index) => {
	let location = `/vtk/oftuku/scene_floor_${zeropadNumber(index)}`;
	let result = await importScene(viewerObject, location);
	result.actors.forEach((actor, index) => {
		viewerObject.actors.push(actor);
		actor.setScale(1, 1, 3);

		let mapper = actor.getMapper();
		let lookupTable = mapper.getLookupTable();
		lookupTable.setSaturationRange([1, 1]);
		if(index == 0){
			lookupTable.setHueRange([1, 1]);
		}
		else if(index == 1){
			lookupTable.setHueRange([0.1666, 0.1666]);
		}
		else if(index == 2){
			lookupTable.setHueRange([0.3333, 0.3333]);
		}
		else if(index == 3){
			lookupTable.setValueRange([0, 0]);
		}
	});
};

const VtkViewer = ({ zoom }) => {
    const viewerElement = React.createRef();
    const [isLoading, setLoading] = React.useState(true);
	const [viewer, setViewer] = React.useState(null);
	const [scene, setScene] = React.useState(0);
	
	const nextScene = () => {
		setScene((scene + 1) % TOTAL_SCENES);
	};

	const prevScene = () => {
		setScene((scene == 0)? (TOTAL_SCENES - 1): (scene - 1));
	};

	const changeScene = (event) => {
		let index = parseInt(event.target.value);
		setScene(index);
	};

	const resetCamera = async () => {
		if(!viewer) return;

		let actors = viewer.actors.splice(0, viewer.actors.length);
		for(let actor of actors) viewer.renderer.removeActor(actor);

		await importFloor(viewer, scene + 1);
		await importPlume(viewer, scene + 1);

		viewer.renderer.setBackground([0.1333, 0.0863, 0.2196]);
		viewer.renderer.resetCamera();
		viewer.camera.zoom(zoom);
		viewer.renderWindow.render();
	};

    React.useEffect(() => {
		setLoading(true);

		(async function(){
			if(!viewer){
				let newViewer = await initViewer(viewerElement.current);
				setViewer(newViewer);

				window.vtkjsViewer = newViewer;
				
				await importTerrain(newViewer);
				await importScene(newViewer, '/vtk/oftuku/chimneys');

				await importFloor(newViewer, scene + 1);
				await importPlume(newViewer, scene + 1);

				newViewer.renderer.setBackground([0.1333, 0.0863, 0.2196]);
				newViewer.renderer.resetCamera();
				//newViewer.renderer.setUseShadows(false);
				newViewer.camera.zoom(zoom);
				//newViewer.camera.azimuth(11);
				newViewer.camera.elevation(-15);

				newViewer.renderWindow.render();
				fixLights(newViewer);
				newViewer.renderWindow.render();
			}
			else{
				resetCamera();
			}

			setLoading(false);
		})().catch(console.error);

		return () => {
			if(viewer){
				let actors = viewer.actors.splice(0, viewer.actors.length);
				for(let actor of actors) viewer.renderer.removeActor(actor);
			}
		}
    }, [scene]);

    return (
		<>
			<div className="vtk-viewer">
				<div className="vtk-viewer-content">
					{(isLoading)&&(
						<div className="loading">
							<LoadingSpinner />
							<span className="loading-label">Cargando Modelo...</span>
						</div>
					)}
					<div className="vtk-viewer-frame" ref={viewerElement}></div>
				</div>
			</div>
			<div className="scenes-navigator">
				<div className="scenes-navigator-item">
					<button className="default-btn not-animated no-icon" onClick={prevScene} disabled={scene == 0}>
						Anterior
					</button>
				</div>
				<div className="scenes-navigator-item">
					<button className="default-btn not-animated no-icon" onClick={nextScene} disabled={scene == (TOTAL_SCENES - 1)}>
						Siguiente
					</button>
				</div>
				<div className="scenes-navigator-item">
					<div className="select-container">
						<select value={scene} onChange={changeScene}>
							{generateOptions()}
						</select>
						<span className="arrow"></span>
					</div>
				</div>
				<div className="scenes-navigator-item">
					<button className="default-btn not-animated no-icon" onClick={resetCamera}>
						<FontAwesomeIcon icon="expand" />
					</button>
				</div>
			</div>
		</>
    )
}

export default VtkViewer;