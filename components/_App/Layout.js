import React from 'react';
import Head from "next/head";
import GoTop from './GoTop';

// Layout

const Layout = ({ children }) => {
    return(
        <React.Fragment>
            <Head>
                <title>MatCom</title>
                <meta name="description" content="MatCom" />
                <meta name="og:title" property="og:title" content="MatCom"></meta>
                <meta name="twitter:card" content="MatCom"></meta>
                <link rel="canonical" href="https://tarn-react.envytheme.com/"></link>
                <meta property="og:image" content="https://res.cloudinary.com/dev-empty/image/upload/v1593069801/explore-learning.jpg" />
            </Head>

            {children}
        
            <GoTop scrollStepInPx="100" delayInMs="10.50" />
        </React.Fragment>
    );
}

export default Layout;