import React from 'react';
import { useRecoilState } from 'recoil'
import { collapsedState } from '../../utils/recoil-atoms'
import Link from '../../utils/ActiveLink';
import clsx from 'clsx';

const Navbar = ({ whiteStyle = false }) => {
    const [collapsed, setCollapsed] = useRecoilState(collapsedState);

    const toggleNavbar = () => {
        setCollapsed(!collapsed);
    }

    React.useEffect(() => {
        let elementId = document.getElementById("navbar");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId.classList.add("is-sticky");
            } else {
                elementId.classList.remove("is-sticky");
            }
        });
        window.scrollTo(0, 0);
    })

    const classOne = collapsed ? 'collapse navbar-collapse' : 'collapse navbar-collapse show';
    const classTwo = collapsed ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

    return (
        <React.Fragment>
            <div id="navbar" className={clsx('navbar-area', whiteStyle && 'white-style')}>
                <div className="tarn-nav">
                    <div className="container-fluid">
                        <nav className="navbar navbar-expand-lg navbar-light">
                            <Link href="/">
                                <a onClick={() => setCollapsed(true)} className="navbar-brand">
                                    <div className="text-logo">MatCom</div>
                                </a>
                            </Link>

                            <button 
                                onClick={toggleNavbar} 
                                className={classTwo}
                                type="button" 
                                data-toggle="collapse" 
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" 
                                aria-expanded="false" 
                                aria-label="Toggle navigation"
                            >
                                <span className="icon-bar top-bar"></span>
                                <span className="icon-bar middle-bar"></span>
                                <span className="icon-bar bottom-bar"></span>
                            </button>

                            <div className={classOne} id="navbarSupportedContent">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <Link href="/" activeClassName="active">
                                            <a onClick={e => e.preventDefault()} className="nav-link">
                                                Home
                                            </a>
                                        </Link>
                                    </li>

                                    <li className="nav-item">
                                        {/* <Link href="/projects" activeClassName="active">
                                            <a onClick={e => e.preventDefault()} className="nav-link">
                                                Proyectos <i className='bx bx-chevron-down'></i>
                                            </a>
                                        </Link> */}
                                        <a onClick={e => e.preventDefault()} className="nav-link">
                                            Proyectos <i className='bx bx-chevron-down'></i>
                                        </a>
                                        
                                        <ul className="dropdown-menu">
                                            <li className="nav-item">
                                                <Link href="/projects/oftuku" activeClassName="active">
                                                    <a onClick={() => setCollapsed(true)} className="nav-link">Oftuku</a>
                                                </Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link href="/projects/huechun" activeClassName="active">
                                                    <a onClick={() => setCollapsed(true)} className="nav-link">Relave</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            
                                {/* <div className="others-option d-flex align-items-center">
                                    <div className="option-item">
                                        <form className="search-box">
                                            <input type="text" className="input-search" placeholder="Search for anything" />
                                            <button type="submit">
                                                <i className="flaticon-loupe"></i>
                                            </button>
                                        </form>
                                    </div>

                                    <div className="option-item">
                                        <a href="/#contact" onClick={() => setCollapsed(true)} className="default-btn">
                                            <i className="flaticon-right"></i> Contactanos <span></span>
                                        </a>
                                    </div>
                                </div> */}
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
}

export default Navbar;
