import React from 'react';
//import Link from 'next/link';

import dictionary from '../../langs/es/footer.json';

const Footer = () => {

    const currentYear = new Date().getFullYear();

    return (
        <footer className="footer-area bg-color">
            <div className="container">
                <div className="row">
                    <div className="col-lg-7 col-sm-12">
                        <div className="single-footer-widget">
                            <a href="/" className="logo">
                                <div className="text-logo">MatCom</div>
                            </a>
                            <p>{dictionary.description}</p>

                            <ul className="social-link">
                                {dictionary.socials.map((social, index) => (
                                    <li key={index}>
                                        {/* <Link href={social.url}>
                                            <a className="d-block" target="_blank">
                                                <i className={'bx bxl-' + social.type}></i>
                                            </a>
                                        </Link> */}
                                        <a className="d-block" href={social.url} target="_blank">
                                            <i className={'bx bxl-' + social.type}></i>
                                        </a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>

                    {/* <div className="col-lg-2 col-sm-6">
                        <div className="single-footer-widget pl-5">
                            <h3>{dictionary.pages.title}</h3>
                            
                            <ul className="footer-links-list">
                                {dictionary.pages.list.map((page, index) => (
                                    <li key={index}>
                                        <Link href={page.url}>
                                            <a>{page.label}</a>
                                        </Link>
                                        <a>{page.label}</a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>

                    <div className="col-lg-2 col-sm-6">
                        <div className="single-footer-widget">
                            <h3>{dictionary.lang.title}</h3>

                            <ul className="footer-links-list">
                                {dictionary.lang.list.map((lang, index) => (
                                    <li key={index}>
                                        <Link href={lang.url}>
                                            <a>{lang.label}</a>
                                        </Link>
                                        <a>{lang.label}</a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div> */}

                    <div className="col-lg-5 col-sm-12">
                        <div className="single-footer-widget">
                            <h3>{dictionary.info.title}</h3>

                            <ul className="footer-contact-info">
                                {/* <li>
                                    <i className='bx bx-map'></i> 
                                    {dictionary.info.address}
                                </li> */}
                                <li>
                                    <i className='bx bx-phone-call'></i>
                                    <a href={dictionary.info.phone.url}>{dictionary.info.phone.content}</a>
                                </li>
                                <li>
                                    <i className='bx bx-envelope'></i>
                                    <a href={dictionary.info.email.url}>{dictionary.info.email.content}</a>
                                </li>
                                {/* <li>
                                    <i className='bx bxs-inbox'></i>
                                    <a href="tel:+557854578964">+55 785 4578964</a>
                                </li> */}
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="footer-bottom-area">
                    <p>Copyright @2020-{currentYear} <strong>MatCom</strong> {dictionary.copyright} <a target="_blank" href="https://matcom.cl/">MatCom.cl</a></p>

                    {/* <div className="col-lg-6 col-md-6">
                        <ul>
                            <li>
                                <Link href="/privacy-policy">
                                    <a>Privacy Policy</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/terms-of-service">
                                    <a>Terms & Conditions</a>
                                </Link>
                            </li>
                        </ul>
                    </div> */}
                </div>
            </div>

            <div className="footer-map">
                <img src="/img/footer-map.png" alt="image" />
            </div>
        </footer>
    );
}

export default Footer;